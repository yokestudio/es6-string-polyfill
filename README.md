# ES6 String Polyfill #

A JavaScript polyfill for ES6 String functions.

Mostly a collection of polyfilled functions retrieved from [MDN](https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/String).

### Set Up ###

1. Download the latest release (as of 10 Jul 2015, v1.0.0).
2. Include in your HTML (preferably just before `</body>`):

    `<script src="es6-string-polyfill-1.0.0.min.js"></script>`

### API ###

Refer to [MDN](https://developer.mozilla.org/en/docs/Web/JavaScript/Reference/Global_Objects/String).

### FAQ ###

1. Does this polyfill have any bugs/limitations?

> `String.raw()` is not polyfilled as it only makes sense to use it with template strings, which is ES6 syntax that cannot be polyfilled correctly and easily.
>
> If you find any other bugs/limitations, please let us know.
   
### Contact ###

* Email us at <yokestudio@hotmail.com>.